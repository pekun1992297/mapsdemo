import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { styleView } from './src/StyleCSS/stylesCSS';


export default class App extends Component {
  render() {
    const {mapView} = styleView;
    return (
      <View style={mapView}>
          <Text>HELLO</Text>
      </View>
    );
  }
}

