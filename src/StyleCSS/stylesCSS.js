import {Dimensions, StyleSheet} from 'react-native';

const {width, height} = Dimensions.get('window');
const heightApp = height-(height*0.44);

export const styleView = StyleSheet.create({
    mapView:{
        width: width,
        height: heightApp,
        flex: 1,
    },
})